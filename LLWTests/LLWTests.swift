//
//  LLWTests.swift
//  LLWTests
//
//  Created by developer on 20/1/2021.
//

import XCTest
@testable import LLW

class LLWTests: XCTestCase {
    private var viewController = ViewController()
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
//  add latest timestamp into addSet first, then add eariler timerstamp into addSet, test whether it is LWW member
    func testing0(){
        viewController.addNewTimeStampToAddSet("A",1)
        viewController.addNewTimeStampToAddSet("A",0)
        let value = viewController.addSet["A"]
        XCTAssertEqual(value, 1)
        let isExist = viewController.isLWWMember("A")
        XCTAssertEqual(isExist, true)
    }
    
//  add same timestamp into addSet, test whether it is LWW member
    func testing1(){
        viewController.addNewTimeStampToAddSet("A",1)
        viewController.addNewTimeStampToAddSet("A",1)
        let value = viewController.addSet["A"]
        XCTAssertEqual(value, 1)
        let isExist = viewController.isLWWMember("A")
        XCTAssertEqual(isExist, true)
    }
    
//  add eariler timestamp into addSet first, then add late timerstamp into addSet, test whether it is LWW member
    func testing2(){
        viewController.addNewTimeStampToAddSet("A",1)
        viewController.addNewTimeStampToAddSet("A",2)
        let value = viewController.addSet["A"]
        XCTAssertEqual(value, 2)
        let isExist = viewController.isLWWMember("A")
        XCTAssertEqual(isExist, true)
    }
    
//  add late timestamp into removeSet first, then add eariler timerstamp into addSet, test whether it is LWW member
    func testing3(){
        viewController.addNewTimeStampToRemoveSet("A",1)
        viewController.addNewTimeStampToAddSet("A",0)
        let valueA = viewController.addSet["A"]
        let valueR = viewController.removeSet["A"]
        var success = false
        if valueA == 0 && valueR == 1 {
            success = true
        }
        XCTAssertEqual(success, true)
        let isExist = viewController.isLWWMember("A")
        XCTAssertEqual(isExist, false)
    }
    
//  add same timestamp into removeSet first, then add same timerstamp into addSet, test whether it is LWW member
    func testing4(){
        viewController.addNewTimeStampToRemoveSet("A",1)
        viewController.addNewTimeStampToAddSet("A",1)
        let valueA = viewController.addSet["A"]
        let valueR = viewController.removeSet["A"]
        var success = false
        if valueA == 1 && valueR == 1 {
            success = true
        }
        XCTAssertEqual(success, true)
        let isExist = viewController.isLWWMember("A")
        XCTAssertEqual(isExist, true)
    }
    
    
//  add earlier timestamp into removeSet first, then add late timerstamp into addSet, test whether it is LWW member
    func testing5(){
        viewController.addNewTimeStampToRemoveSet("A",1)
        viewController.addNewTimeStampToAddSet("A",2)
        let valueA = viewController.addSet["A"]
        let valueR = viewController.removeSet["A"]
        var success = false
        if valueA == 2 && valueR == 1 {
            success = true
        }
        XCTAssertEqual(success, true)
        let isExist = viewController.isLWWMember("A")
        XCTAssertEqual(isExist, true)
    }
    
    
//  add late timestamp into removeSet first, then add earlier timerstamp into removeSet, test whether it is LWW member
    func testing0r(){
        viewController.addNewTimeStampToRemoveSet("A",1)
        viewController.addNewTimeStampToRemoveSet("A",0)
        let value = viewController.removeSet["A"]
        XCTAssertEqual(value, 1)
        let isExist = viewController.isLWWMember("A")
        XCTAssertEqual(isExist, false)
    }
    
//  add same timestamp into removeSet first, then add same timerstamp into removeSet, test whether it is LWW member
    func testing1r(){
        viewController.addNewTimeStampToRemoveSet("A",1)
        viewController.addNewTimeStampToRemoveSet("A",1)
        let value = viewController.removeSet["A"]
        XCTAssertEqual(value, 1)
        let isExist = viewController.isLWWMember("A")
        XCTAssertEqual(isExist, false)
    }
    
//  add earlier timestamp into removeSet first, then add late timerstamp into removeSet, test whether it is LWW member
    func testing2r(){
        viewController.addNewTimeStampToRemoveSet("A",1)
        viewController.addNewTimeStampToRemoveSet("A",2)
        let value = viewController.removeSet["A"]
        XCTAssertEqual(value, 2)
        let isExist = viewController.isLWWMember("A")
        XCTAssertEqual(isExist, false)
    }
    
//  add late timestamp into addSet first, then add earlier timerstamp into removeSet, test whether it is LWW member
    func testing3r(){
        viewController.addNewTimeStampToAddSet("A",1)
        viewController.addNewTimeStampToRemoveSet("A",0)
        let valueA = viewController.addSet["A"]
        let valueR = viewController.removeSet["A"]
        var success = false
        if valueA == 1 && valueR == 0 {
            success = true
        }
        XCTAssertEqual(success, true)
    }
    
//  add same timestamp into addSet first, then add same timerstamp into removeSet, test whether it is LWW member
    func testing4r(){
        viewController.addNewTimeStampToAddSet("A",1)
        viewController.addNewTimeStampToRemoveSet("A",1)
        let valueA = viewController.addSet["A"]
        let valueR = viewController.removeSet["A"]
        var success = false
        if valueA == 1 && valueR == 1 {
            success = true
        }
        XCTAssertEqual(success, true)
        let isExist = viewController.isLWWMember("A")
        XCTAssertEqual(isExist, true)
    }
    
//  add earlier timestamp into addSet first, then add late timerstamp into removeSet, test whether it is LWW member
    func testing5r(){
        viewController.addNewTimeStampToAddSet("A",1)
        viewController.addNewTimeStampToRemoveSet("A",2)
        let valueA = viewController.addSet["A"]
        let valueR = viewController.removeSet["A"]
        var success = false
        if valueA == 1 && valueR == 2 {
            success = true
        }
        XCTAssertEqual(success, true)
        let isExist = viewController.isLWWMember("A")
        XCTAssertEqual(isExist, false)
    }
    
    func testingMerge(){
        viewController.addNewTimeStampToAddSet("A",1)
        viewController.addNewTimeStampToRemoveSet("A",2)
        viewController.addNewTimeStampToAddSet("B",1)
        viewController.addNewTimeStampToAddSet("C",1)
        viewController.addNewTimeStampToRemoveSet("C",0)
        
        let mergeSet = viewController.merge()
        XCTAssertEqual(mergeSet.count, 2)
        XCTAssertEqual(mergeSet[0], "B")
        XCTAssertEqual(mergeSet[1], "C")
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
