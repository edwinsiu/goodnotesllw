//
//  ViewController.swift
//  LLW
//
//  Created by developer on 20/1/2021.
//

import UIKit

class ViewController: UIViewController {
    var addSet = [String:Int]()
    var removeSet = [String:Int]()
    
    let addLock = NSLock()
    let removeLock = NSLock()
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    func addNewTimeStampToAddSet(_ key: String, _ value: Int){
        addLock.lock()
        if let oldValue = addSet[key] {
            if oldValue > value {
                return
            }
        }
        addSet[key] = value
        addLock.unlock()
    }
    
    func addNewTimeStampToRemoveSet(_ key: String, _ value: Int){
        removeLock.lock()
        if let oldValue = removeSet[key] {
            if oldValue > value {
                return
            }
        }
        removeSet[key] = value
        removeLock.unlock()
    }
    
    func merge()->[String]{
        var mergeSet = [String]()
        for (key, _) in addSet {
            if isLWWMember(key) {
                mergeSet.append(key)
            }
        }
        return mergeSet
    }
    
    func isLWWMember(_ key:String)->Bool{
        if let addValue = addSet[key] { // if it is in the add set
            guard let removeValue = removeSet[key] else {
                return true // not in the remove set
            }
            if addValue >= removeValue {
                // remove set but with an earlier tamestamp than the latest time stamp in the add set
                return true
            } else {
                return false
            }
        } else {
            return false
        }
    }
}

